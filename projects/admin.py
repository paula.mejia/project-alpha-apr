from django.contrib import admin
from .models import Project
from tasks.models import Task


@admin.register(Project)
class Project(admin.ModelAdmin):
    list_display = ("name", "description", "owner")


@admin.register(Task)
class Task(admin.ModelAdmin):
    list_display = (
        "name",
        "start_date",
        "due_date",
        "is_completed",
        "project",
        "assignee",
    )
